#!/usr/bin/env python3

from i3bfutils import (template, delay, io)
import re

MEMINFO_RE = re.compile(
    r"""
    MemTotal:\s+(\d+).*\n
    MemFree:\s+(\d+).*\n
    .*\n
    Buffers:\s+(\d+).*\n
    Cached:\s+(\d+).*\n
    """
    , re.MULTILINE | re.VERBOSE)


def get_mem_pct_from_meminfo():
    # RealUsed = Total - (Cache + Buff + Free)
    with open('/proc/meminfo', 'r') as file:
        data = file.read()

    total, free, buffers, cached = (int(x) for x in MEMINFO_RE.findall(data)[0])
    used = total - free - buffers - cached
    return round(used / (total / 100.0))


dflt_layout = ('[RAM {{ram_usage_pct}}'
               '|value={{_ram_usage_pct}}:min=0:max=100:'
               'scale={SCALE}:{COLORS}]')
cfg = {
    'COLORS': io.get_var('COLORS', default='#F20-#09B'),
    'SCALE': io.get_var('SCALE', default='linear', options=('linear', 'sqrt', 'log')).lower(),
    'INTERVAL': io.get_var('INTERVAL', default=1),
    'LAYOUT': io.get_var('LAYOUT', default=dflt_layout)
}

if cfg['LAYOUT'] == dflt_layout:
    cfg['LAYOUT'] = cfg['LAYOUT'].format(**cfg)

info = template.PrettyDict(prettifiers={
    'ram_usage_pct': lambda t: '{:d} %'.format(t),
})

tmplt = template.Template(cfg['LAYOUT'])
info['_ram_usage_pct'] = get_mem_pct_from_meminfo()

io.push(tmplt.make_blocks(info, init=True))

while True:
    delay.sleep(cfg['INTERVAL'])

    new_battery_pct = get_mem_pct_from_meminfo()

    if info['_ram_usage_pct'] != new_battery_pct:
        info['_ram_usage_pct'] = new_battery_pct
        io.push(tmplt.make_blocks(info))
