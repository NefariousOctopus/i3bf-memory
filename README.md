# i3bf-memory - i3barfodder memory plugin
A simple [i3barfodder](https://gitlab.com/rndusr/i3barfodder) memory plugin.


## Disclaimer

This is fast dirty 'works-for-me' script. It can do anything from "running just fine" to "launching full scale nulcear attack on random nation". Use on your own risk! :)


## Install

Just put the script somewhere in your `$PATH` and make it executable. The following snipper asumes that `~/bin` is in your `$PATH`.

```bash
$ git clone git@gitlab.com:NefariousOctopus/i3bf-memory.git
$ cd i3bf-memory
$ chmod u+x ./i3bf-memory
$ cp i3bf-memory ~/bin
```


## Configuration

Example config.

```
[memory]
command = i3bf-memory
COLORS = #F20-#09B
```

For more see [original project](https://gitlab.com/rndusr/i3barfodder).

## Status

- Currently getting the data by regex-ing the contents of `/proc/meminfo`
